// Ylikuutio - A 3D game and simulation engine.
//
// Copyright (C) 2015-2020 Antti Nuortimo.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __SPECIES_OR_GLYPH_HPP_INCLUDED
#define __SPECIES_OR_GLYPH_HPP_INCLUDED

#include "entity.hpp"
#include "render_templates.hpp"
#include "code/ylikuutio/opengl/opengl.hpp"

// Include GLEW
#include "code/ylikuutio/opengl/ylikuutio_glew.hpp" // GLfloat, GLuint etc.

namespace yli
{
    namespace ontology
    {
        template<class T1>
            void render_species_or_glyph(T1 const species_or_glyph_pointer)
            {
                // Compute the MVP matrix from keyboard and mouse input.
                glUniform3f(
                        species_or_glyph_pointer->lightID,
                        species_or_glyph_pointer->light_position.x,
                        species_or_glyph_pointer->light_position.y,
                        species_or_glyph_pointer->light_position.z);

                // 1st attribute buffer: vertices.
                yli::opengl::enable_vertex_attrib_array(species_or_glyph_pointer->vertex_position_modelspaceID);

                // 2nd attribute buffer: UVs.
                yli::opengl::enable_vertex_attrib_array(species_or_glyph_pointer->vertexUVID);

                // 3rd attribute buffer: normals.
                yli::opengl::enable_vertex_attrib_array(species_or_glyph_pointer->vertex_normal_modelspaceID);

                // Render this `Species` or `Glyph` by calling `render()` function of each `Object`.
                yli::ontology::render_children<yli::ontology::Entity*>(species_or_glyph_pointer->parent_of_objects.child_pointer_vector);

                yli::opengl::disable_vertex_attrib_array(species_or_glyph_pointer->vertex_position_modelspaceID);
                yli::opengl::disable_vertex_attrib_array(species_or_glyph_pointer->vertexUVID);
                yli::opengl::disable_vertex_attrib_array(species_or_glyph_pointer->vertex_normal_modelspaceID);
            }
    }
}

#endif
