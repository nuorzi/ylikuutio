Hirvi 0.0.4 / Ajokki 0.0.4 / Ylikuutio 0.0.4 manual.

Ylikuutio is a 3D game and simulation engine.
It is designed with two different game types in mind:
1. real-time action adventures (either first-person or third-person).
2. turn-based tactical games.

Other design goals include support also for slower video cards, and
Linux, Windows, and Android as target operating systems (Android support
is not implemented yet). Only 64-bit systems are supported.
OpenGL 3.0 or newer is required.

Ylikuutio can be compiled with GCC, Clang or Visual Studio.
In Visual Studio compiling may break due to compiler-specific bugs.
Cross compiling from Linux® to Windows using MinGW-w64 works fine.
C++14 support is required.
CMake 2.6.2 or newer is needed for the compiling process, but
external libraries such as `glm` require CMake 3.2 or a newer version.
CMake uses git for downloading Google Test testing framework.

The present document explains some design choices of Ylikuutio. To
understand how to use Ylikuutio in practice, Hirvi, Ajokki and Ylikuutio
code itself are the best references on that. The information in the
present manual reflects the current state of development of Ylikuutio.

Ylikuutio has an ontological hierarchy (class hierarchy), consisting of
classes most of which belong to namespace `yli::ontology`. The top
class of this ontological hierarchy is `yli::ontology::Universe`. Each
class is responsible of the lifetime of its children. The hierarchy is
implemented using function templates belonging to `yli::hierarchy`.
This means that the destructor of each `yli::ontology` class must
`delete` all its children. `yli::ontology::EntityFactory` is available
by calling `Universe::get_entity_factory` member function. This is the
recommended way to create entities, because it takes care of providing
`yli::ontology::Universe` pointer to the constructor of new `Entity`.

To initialize the ontological hierarchy, `yli::ontology::Universe` needs
to be created. After that an `EntityFactory` should be requested from
`yli::ontology::Universe` using `Universe::get_entity_factory` and the
rest of the entities should be created using this `EntityFactory`.

Some of the `yli::ontology` classes have a more complex position in the
ontological hierarchy. For example, `yli::ontology::Symbiosis` and the
classes related to it. Please see the ontological class diagram.

Keyboard and mouse handling is based on callbacks.
TODO: implement mouse handling using callbacks.
Console command processing is based on callbacks.
Game event processing will be based on callbacks.

Hirvi

Hirvi is a first person action-adventure of the adventures of
a moose/elk called Hirvi. Cities are dangerous places for moose,
even through moose are well-known pasifists.
"hirvi" means a moose/elk in Finnish.
Hirvi is a work in progress.

F1 to get help.
` to enter/exit console.

Console commands:

To activate a scene or a camera:
`activate`

To get info about the entities:
`info`

To set the value of an existing variable:
`set`

To get the value of a variable:
`get`

To delete an entity (and all its descendants):
`delete`

To exit Hirvi:
`bye`
`chau`
`ciao`
`heippa`
`sayonara`

To evaluate s7 Scheme code:
`eval`

Ajokki

Ajokki is a demo program for testing Ylikuutio.

In Ajokki all console commands of Hirvi work, but in addition there
are some console commands for debugging use as well. Therefore, Hirvi
reflects actual game development, whereas Ajokki showcases different
functionalities of Ylikuutio.

External libraries

Assimp is to be used only in `yli_convert` file conversion tool.
The actual asset loading is done using OpenFBX for `.fbx` files and
custom file loaders for other supported file formats.
In the future Assimp will probably be replaced by a custom FBX writer.
